import sys
import PyQt5.QtWidgets as QtWidgets
import ui.window as _window
from neovim import attach


def spawn_nvim():
    nvim_argv = ['nvim', '--embed']
    nvim = attach('child', argv=nvim_argv)
    print(nvim)
    return nvim


def main():
    app = QtWidgets.QApplication(sys.argv)
    nvim = spawn_nvim()
    window = _window.NeovimWindow(nvim)
    window.show()
    app.exec_()

if __name__ == '__main__':
    main()
